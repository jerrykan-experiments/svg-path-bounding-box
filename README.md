# Proof of Concept: SVG Path Bound Box

A basic proof-of-concept for determining the bounding box of a SVG path.

The partial implementation includes the following:
  - SVG `d` directions parser for the `M`/`m`, `L`/`l`, `H`/`h`, `V`/`v`,
    `C`/`c`, and `Z`/`/z` commands.
  - Determining the bounding boxes of line, and cubic bezier curve segments

Except for the `A`/`a` directions, the code for parsing the other SVG `d`
directions exists, but no testing has been done, so those code paths have been
stubbed out. In theory they should be mostly correct.

The decision to store `Point` values as `Decimals` was based on the desire to
have bounding box values be as closely accurate to the original values in the
SVG files, without introducing rounding errors caused by float values.


## References
  - [SVG `d` attribute](https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/d)
  - [Cubic bezier curve Bounding box algorithm](https://stackoverflow.com/questions/2587751/an-algorithm-to-find-bounding-box-of-closed-bezier-curves/14429749#14429749) - implementation based on "Code 1" section.
