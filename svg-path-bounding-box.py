#!/usr/bin/env python3

import math
from decimal import Decimal
from pprint import pprint

path = """
m 194.65337,159.94901
c -1.37118,7.05412 -12.28069,14.77419 -24.81045,16.27036
  -6.5336,0.77957 -12.96659,1.49609 -19.82626,1.18144
  -11.21818,-0.51393 -20.07026,-2.67761 -20.07026,-2.67761
  0,1.09207 0.0673,2.13192 0.2022,3.10444
  1.45837,11.0712 10.97792,11.73435 19.9954,12.04356
  9.10141,0.31143 17.20553,-2.24395 17.20553,-2.24395
l 0.37379,8.22812
c 0,0 -6.36594,3.41854 -17.70673,4.04728
  -6.25339,0.34355 -14.01821,-0.15723 -23.06203,-2.55115
  -19.61438,-5.19164 -22.97325,-26.09999 -23.50382,-47.31424
  -0.15723,-6.29878 -0.0605,-12.2382 -0.0601,-17.20568
  6.8e-4,-21.69304 14.21334,-28.05169 14.21334,-28.05169
  7.16657,-3.29137 19.46407,-4.67544 32.2486,-4.779906
h 0.31408
c 12.78446,0.104696 25.08991,1.488536 32.25613,4.779906
  0,0 14.21278,6.3586 14.21278,28.05169
  0,0 0.17839,16.00527 -1.98214,27.11743
z
M 179.8704,134.51449
c 0,-5.36837 -1.31848,-9.67704 -4.1119,-12.79043
  -2.83174,-3.15618 -6.53961,-4.774 -11.14177,-4.774
  -5.32581,0 -9.35846,2.04669 -12.02427,6.14087
l -2.59258,4.34514
  -2.59204,-4.3451
c -2.66638,-4.09426 -6.6991,-6.14106 -12.02432,-6.14106
  -4.60271,0 -8.31059,1.61794 -11.14179,4.77407
  -2.74554,3.15622 -4.1124,7.42214 -4.1124,12.79044
v 26.2667
h 10.40629
v -25.49454
c 0,-5.37431 2.26116,-8.10204 6.78416,-8.10204
  5.00093,0 7.50777,3.23577 7.50777,9.63425
v 13.95461
h 10.34504
v -13.95453
c 0,-6.39848 2.50636,-9.63425 7.50721,-9.63425
4.5229,0 6.78422,2.72769 6.78422,8.10203
v 25.49462
h 10.40627
v -26.26674
z
""".strip()

cmds_moveto = ['M', 'm']
cmds_line_to = ['L', 'l', 'H', 'h', 'V', 'v']
cmds_cubic_bezier_curve = ['C', 'c', 'S', 's']
cmds_quad_bezier_curve = ['Q', 'q', 'T', 't']
cmds_elliptical_arc_curve = ['A', 'a']
cmds_close_path = ['Z', 'z']

path_commands = (
    cmds_moveto
    + cmds_line_to
    + cmds_cubic_bezier_curve
    + cmds_quad_bezier_curve
    + cmds_elliptical_arc_curve
    + cmds_close_path
)


def _decimal_ceil(value, decimals=5):
    return Decimal(math.ceil(Decimal(value) * 10**decimals)) / 10**decimals


def _decimal_floor(value, decimals=5):
    return Decimal(math.floor(Decimal(value) * 10**decimals)) / 10**decimals


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    @staticmethod
    def from_str(point_str):
        return Point(*[Decimal(v) for v in point_str.split(',')])

    def __getitem__(self, key):
        return getattr(self, key)

    def __add__(self, other):
        return Point(self.x + other.x, self.y + other.y)

    def __mul__(self, other):
        # raise error if not a number?
        return Point(self.x * other, self.y * other)

    def __div__(self, other):
        return Point(self.x / other, self.y / other)

    def __str__(self):
        return '({0.x}, {0.y})'.format(self)

    def __repr__(self):
        return 'Point{}'.format(self)


class Line:
    def __init__(self, start, end):
        self.start = start
        self.end = end

    def __repr__(self):
        return 'Line(start={}, end={}'.format(self.start, self.end)

    def bounding_box(self):
        return {
            'min': Point(
                _decimal_floor(min(self.start.x, self.end.x)),
                _decimal_floor(min(self.start.y, self.end.y)),
            ),
            'max': Point(
                _decimal_ceil(max(self.start.x, self.end.x)),
                _decimal_ceil(max(self.start.y, self.end.y)),
            ),
        }


class QuadraticBezier:
    def __init__(self, start, control, end):
        self.start = start
        self.control = control
        self.end = end

    def __repr__(self):
        return 'QuadraticBezier(start={}, control={}, end={}'.format(
            self.start, self.control, self.end
        )


class CubicBezier:
    def __init__(self, start, control1, control2, end):
        self.start = start
        self.control1 = control1
        self.control2 = control2
        self.end = end

    def __repr__(self):
        return 'CubicBezier(start={}, control1={}, control2={}, end={}'.format(
            self.start, self.control1, self.control2, self.end
        )

    def bounding_box(self):
        tvalues = []

        for axis in ['x', 'y']:
            a = (
                (-3 * self.start[axis])
                + (9 * self.control1[axis])
                - (9 * self.control2[axis])
                + (3 * self.end[axis])
            )
            b = (
                (6 * self.start[axis])
                - (12 * self.control1[axis])
                + (6 * self.control2[axis])
            )
            c = (3 * self.control1[axis]) - (3 * self.start[axis])

            if abs(a) < 1e-12:
                if abs(b) < 1e-12:
                    continue

                t = -c / b
                if 0 < t < 1:
                    tvalues.append(t)

                continue

            # b^2 - 4ac
            b2ac = (b * b) - (4 * c * a)
            if b2ac < 0:
                continue

            sqrtb2ac = Decimal(math.sqrt(b2ac))

            t1 = (-b + sqrtb2ac) / (2 * a)
            if 0 < t1 < 1:
                tvalues.append(t1)

            t2 = (-b - sqrtb2ac) / (2 * a)
            if 0 < t2 < 1:
                tvalues.append(t2)

        values = {
            'x': [],
            'y': [],
        }

        for t in tvalues:
            mt = 1 - t
            for axis in ['x', 'y']:
                values[axis].append(
                    (mt * mt * mt * self.start[axis])
                    + (3 * mt * mt * t * self.control1[axis])
                    + (3 * mt * t * t * self.control2[axis])
                    + (t * t * t * self.end[axis])
                )

        values['x'] += [self.start.x, self.end.x]
        values['y'] += [self.start.y, self.end.y]

        return {
            'min': Point(
                _decimal_floor(min(values['x'])),
                _decimal_floor(min(values['y'])),
            ),
            'max': Point(
                _decimal_floor(max(values['x'])),
                _decimal_floor(max(values['y'])),
            ),
        }

#    @staticmethod
#    def from_quad(start, control, end):
#        raise NotImplemented("Untested code path")
#        return CubicBezier(
#            start,
#            start + ((start - control) * 2 / 3)
#            end + ((end - control) * 2 / 3)
#            end,
#        )


current_pos = Point(0, 0)
start_pos = None
directions = path.split()
segments = []

while directions:
    if directions[0] in path_commands:
        cmd = directions.pop(0)
    else:
        if cmd == 'M':
            cmd = 'L'
        elif cmd == 'm':
            cmd = 'l'

    if cmd in cmds_moveto:
        point = Point.from_str(directions.pop(0))
        if cmd == 'M':
            current_pos = point
        else:  # m
            current_pos += point
        continue
    elif cmd in cmds_line_to:
        if cmd in ['L', 'l']:
            point = Point.from_str(directions.pop(0))
            if cmd == 'L':
                segment = Line(current_pos, point)
            else:  # l
                segment = Line(current_pos, current_pos + point)
        elif cmd in ['H', 'h']:
            x = Decimal(directions.pop(0))
            if cmd == 'H':
                segment = Line(current_pos, Point(x, current_pos.y))
            else:  # h
                segment = Line(current_pos, current_pos + Point(x, 0))
        else:  # V, v
            y = Decimal(directions.pop(0))
            if cmd == 'V':
                segment = Line(current_pos, Point(current_pos.x, y))
            else:  # v
                segment = Line(current_pos, current_pos + Point(0, y))

        current_pos = segment.end
        print(segment, segment.bounding_box())
        segments.append(segment)
    elif cmd in cmds_cubic_bezier_curve:
        if cmd in ['C', 'c']:
            # Cubic bezier curve
            if cmd == 'C':
                segment = CubicBezier(
                    current_pos,
                    Point.from_str(directions.pop(0)),
                    Point.from_str(directions.pop(0)),
                    Point.from_str(directions.pop(0)),
                )
            else:  # c
                segment = CubicBezier(
                    current_pos,
                    current_pos + Point.from_str(directions.pop(0)),
                    current_pos + Point.from_str(directions.pop(0)),
                    current_pos + Point.from_str(directions.pop(0)),
                )
        else:  # S, s
            raise NotImplementedError('Untested code path')
            # Smooth cubic bezier curve
            try:
                prev_seg = segment[-1]
                if isinstance(prev_seg, CubicBezier):
                    # reflect control point
                    control1 = (
                        prev_seg.end
                        + (prev_seg.end - prev_seg.control2)
                    )
                else:
                    control1 = directions[0]
            except IndexError:
                control1 = directions[0]

            if cmd == 'S':
                segment = CubicBezier(
                    current_pos,
                    control1,
                    Point.from_str(directions.pop(0)),
                    Point.from_str(directions.pop(0)),
                )
            else:  # s
                segment = CubicBezier(
                    current_pos,
                    control1,
                    current_pos + Point.from_str(directions.pop(0)),
                    current_pos + Point.from_str(directions.pop(0)),
                )

        current_pos = segment.end
        print(segment, segment.bounding_box())
        segments.append(segment)
    elif cmd in cmds_quad_bezier_curve:
        raise NotImplementedError('Untested code path')
        if cmd in ['Q', 'q']:
            if cmd == 'Q':
                segment = QuadraticBezier(
                    current_pos,
                    Point.from_str(directions.pop(0)),
                    Point.from_str(directions.pop(0)),
                )
            else:  # q
                segment = QuadraticBezier(
                    current_pos,
                    current_pos + Point.from_str(directions.pop(0)),
                    current_pos + Point.from_str(directions.pop(0)),
                )
        else:  # T, t
            try:
                prev_seg = segment[-1]
                if isinstance(prev_seg, QuadraticBezier):
                    # reflect control point
                    control = prev_seg.end + (prev_seg.end - prev_seg.control)
                else:
                    control = directions[0]
            except IndexError:
                control = directions[0]

            if cmd == 'T':
                segment = QuadraticBezier(
                    current_pos,
                    control,
                    Point.from_str(directions.pop(0)),
                )
                current_pos = point
            else:  # t
                segment = QuadraticBezier(
                    current_pos,
                    control,
                    current_pos + Point.from_str(directions.pop(0)),
                )

        current_pos = segment.end
        print(segment)
        segments.append(segment)
    elif cmd in cmds_elliptical_arc_curve:
        raise NotImplementedError(
            "Parsing  elliptical arc curves not yet implemented")
        if cmd == 'A':
            pass
        else:  # a
            pass
    elif cmd in cmds_close_path:
        current_pos = start_pos
        start_pos = None
    else:
        breakpoint()
        raise NotImplementedError('undefined code path')

    if cmd not in cmds_close_path and start_pos is None:
        start_pos = segments[-1].start

segment_bboxes = [s.bounding_box() for s in segments]

pprint({
    'min': Point(
        min(p['min'].x for p in segment_bboxes),
        min(p['min'].y for p in segment_bboxes),
    ),
    'max': Point(
        max(p['max'].x for p in segment_bboxes),
        max(p['max'].y for p in segment_bboxes),
    ),
})
